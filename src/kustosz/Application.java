package kustosz;

import java.awt.TextField;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import javax.swing.JFrame;

public class Application {
	
	private static List<Product> products = new ArrayList<>();		//lists of products
	public static List<Product> shopingList = new ArrayList<>();	//shoping list
	
	public static long barCode;
	private static int input;
	public static int counter = 0;
	public static String productInfo = "";
	private static Scanner s = new Scanner(System.in);
	
/////////////////////////////////////magazin////////////////////////////////////////////
	static
	{
		products.add(new Product("cebula" ,    1111111110, 1 , 2.99));
		products.add(new Product("marchewka",  1110000111, 1 , 0.99));
		products.add(new Product("jablka",     1111111111, 1 , 4.99));
		products.add(new Product("buraki",     0101010100, 1 , 5.79));
		products.add(new Product("pomarancze", 1110110111, 1 , 1.23));
		products.add(new Product("banany",     1000000001, 1 , 3.21));
	}
//////////////////////////////////////////////////////////////////////////////////
	
	
	public static void main(String[] args) {
		
		Gui gui = new Gui();
		gui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		gui.setVisible(true);
			
		do{
		
			showListOfProductsAvailiable();	//show products list
			showMenu();
			
			input = s.nextInt();
			
			switch (input) {
			
			case 1:
				scanBarCode();
				ifProductIsAvailable(counter);	
				break;
				
			case 2:
				//System.out.println(" Print Receipt is not implemented yet");
				printReceipt();
				break;

			default:
				System.out.println("there is no such option, try again");
				break;
			}

		}while(true);
		
	}
	
////////////////////////////////////////////////////////////////////////////////////////////
//	private static void createWindow() {
//		Gui window = new Gui();
//		
//	}
////////////////////////////////////////////////////////////////////////////////////////////
	public static String ifProductIsAvailable(int counter) {
		
	  for (Product p : products) {
		//iteruje przez liste i szukam czy wprowadzony barCode 
		//jest rowny temu w "magazynie" na liscie	
		if( barCode == p.getBarCode() ){
			
			System.out.println("Product is avaliable");
			//System.out.println("\t\t--->"+p.toString());
			shopingList.add(p);				//add product to shopping list of indywidual purchase
			
			return productInfo+="product is available:"+p.toString()+"\n";
		}
		
		if( barCode != p.getBarCode() ){
			counter++;
		}
	}
	  
	if( counter == products.size() ){
		//jesli przeiterowal wszystkie i nie znalazl
		//productInfo="";
		System.out.println("Product with BarCode: "+barCode+""
				+ "\n is NOT found in memory/magazine");
		
		return productInfo+="Product with BarCode: " + barCode + "\n is NOT found in memory/magazine"+"\n";
	}
		//productInfo="";
		
	return "";
}
////////////////////////////////////////////////////////////////////////////////////////	
	public static double printReceipt() {
		
		double priceTotal = 0;
		
		System.out.println("***your purchase list***");
		
		for (Product p : shopingList) {
			System.out.println(p.toString());
			priceTotal+=p.getPriceForOneProduct();
		}
		System.out.println("\t\t--->TotalPrice for scaanned products = "+priceTotal+"\n\n");
		//priceTotal = 0;			//po wydrukowaniu ceny za zeskanowane produkty zeruje t� sume
		
		return priceTotal;
	}
//////////////////////////////////////////////////////////////////////////////////
	private static void showMenu() {
		
		System.out.println("\n*************************************************");
		System.out.println("******WELCOME IN THE SALE POINT APPLICATION******");
		System.out.println("***************************************************\n");
		System.out.println("1---> ***Scan New Item***");
		System.out.println("2---> ***Print Receipt***");
		System.out.println("***************************************************");
	}
///////////////////////////////////////////////////////////////////////////////////
	private static void scanBarCode() {
		
		
		try {
			System.out.println("Scan/Enter 10 Digits BarCode, for example: 1010101111");
			barCode = BarCodeScanner.readLong(); //przy odczycie z konsoli
				//barCode = Long.parseLong(gui.texField.getText());
				//wpi�cie sie z GUI-em i TexFildem pod aplikacje konsolow�
			System.out.println("Scaned barCode_fromeConsole: " + barCode);
		} catch (Exception e) {
			System.out.println("podaj wlasciwy barCode");
		}
		
	}
	
///////////////////////////////////////////////////////////////////////////////////

	private static void showListOfProductsAvailiable() {
		
		System.out.println("***************************************************************"
				+ "\nList Of Products: ");
		
		for (Product prod : products) {
			System.out.println(prod.toString());
		}
		System.out.println("****End List Of Products:*****");
	}
}
