package kustosz;

public class Product {

	private String name;
	private long barCode;		//sequence of 0 and 1 f.e 0100011111010
	private int amount;	
	private double priceForOneProduct;

	
	public Product(String name, long barCode, int amount, double priceForOneProduct) {
		
		this.name = name;
		this.barCode = barCode;
		this.amount = amount;
		this.priceForOneProduct = priceForOneProduct;
	}

//////////////////////////////////i am not sure do i reely need all geters and seters
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getBarCode() {
		return barCode;
	}

	public void setBarCode(long barCode) {
		this.barCode = barCode;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public double getPriceForOneProduct() {
		return priceForOneProduct;
	}

	public void setPriceForOneProduct(double priceForOneProduct) {
		this.priceForOneProduct = priceForOneProduct;
	}
	
	@Override
	public String toString() {
		return "Product [name=" + name + ", barCode=" + barCode + ", amount=" + amount + ", priceForOneProduct="
				+ priceForOneProduct + "]";
	}
	
}
