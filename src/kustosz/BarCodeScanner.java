package kustosz;

import java.util.Scanner;

public class BarCodeScanner {

	public static String readLine() {
        try {
            return new Scanner(System.in).nextLine();
        } catch (Exception e) {
            return null;
        }
    }

	public static int readInt() {
        try {
            return new Scanner(System.in).nextInt();
        } catch (Exception e) {
            return 1;
        }
    }

	public static double readDouble() {
        try {
            return new Scanner(System.in).nextDouble();
        } catch (Exception e) {
            return 1.0;
        }
    }
	
	public static long readLong() {
        try {
            return new Scanner(System.in).nextLong();
        } catch (Exception e) {
            return 1;
        }
    }

}
