package kustosz;

import java.awt.Font;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class Gui extends JFrame implements ActionListener {
	
	JTextField texField;
	JLabel label, labelPlace;
	JButton butonSkanuj;
	ShopingListGUI sl;
	JButton butonPlac;

	public Gui()
	{
	  setSize(700,600);
	  setTitle("***KustoszLabs***");
	  setLayout(null);
	  
	  butonSkanuj = new JButton("skanuj");
	  butonSkanuj.setBounds(100, 200, 500, 50);
	  butonSkanuj.addActionListener(this);	// zrodlo zdarzen to buton a sluchacz to Gui
	  add(butonSkanuj);	//dodaje buutona do ramki
	  
	  texField = new JTextField();
	  texField.setBounds(100, 100, 500, 50);
	  add(texField);
	  
//	  label = new JLabel("test");
//	  label.setBounds(20, 350, 1000, 200);
//	  add(label);
	  
	  butonPlac = new JButton("P�ac�");
	  butonPlac.setBounds(100, 300, 500, 50);
	  butonPlac.addActionListener(this);	// zrodlo zdarzen to buton a sluchacz to Gui
	  add(butonPlac);	//dodaje buutona do ramki
	  
	  labelPlace = new JLabel("naleznosc:");
	  labelPlace.setBounds(290, 400, 1000, 200);
	  add(labelPlace);
	  
	  
	  sl = new ShopingListGUI();
	}

	@Override		//w scopie tej metody mowimy co ma sie wykonywac
	public void actionPerformed(ActionEvent e) {
		
		Object source = e.getSource();
		
		if(source==butonSkanuj){
		
			try {
				
				 
				  sl.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				  sl.setLocationRelativeTo(null);
				  sl.setVisible(true);
				
				Application.barCode = Long.parseLong(texField.getText()); //barCode is a long type
				//Application.ifProductIsAvailable(Application.counter);
				//label.setText(Application.ifProductIsAvailable(Application.counter));//setText produktu do labelki
				texField.setText("");
				//dodawanie do listy zakupow graficzne
				ShopingListGUI.texArea.setText(Application.ifProductIsAvailable(Application.counter)+"\n");	
			}
			catch (Exception e2) {
				System.out.println("podaj wlasciwy barCode");
				label.setText("podaj wlasciwy barCode");
				//label.setText(Application.ifProductIsAvailable(Application.counter));
			}
		}
		else if(source==butonPlac){
			
			//System.out.println("bede placic:");
			//System.out.println("do zaplaty= "+Application.printReceipt());
			texField.setText("***ZAPLACONO***");
			labelPlace.setText("DO ZAPLATY: = "+Application.printReceipt()+" PLN");
			Application.shopingList.clear();	//czysci liste po wydrukowaniu paragonu i mozna skanowac nastepn� sesj�
			sl.dispose();
				
		}
	}
}